/**
 * @file
 * Contains the definition of the behavior speedboxes.
 */

(($, Drupal) => {
  Drupal.speedboxes = Drupal.speedboxes || {};

  // Mouse coordinates in arrays are always X, Y.
  Drupal.speedboxes.bMouseDown = false;
  Drupal.speedboxes.bMouseMoved = false;
  Drupal.speedboxes.aMouseStartPosition = [0, 0];
  Drupal.speedboxes.aMouseLastPosition = [0, 0];
  Drupal.speedboxes.pSelectionRect = null;
  Drupal.speedboxes.pSelectionContainer = null;
  Drupal.speedboxes.pSelectedCheckboxes = null;
  Drupal.speedboxes.pEditingPopup = null;
  Drupal.speedboxes.config = {
    ignore_elements: ['input', 'select'],
    localization: {
      check_all: Drupal.t('Check all'),
      uncheck_all: Drupal.t('Uncheck all'),
      reverse: Drupal.t('Reverse selection'),
    },
  };

  /**
   * Attaches the JS behavior to apply Speedboxes JS.
   */
  Drupal.behaviors.speedboxes = {
    attach(context) {
      $('body', context)
        .mousedown((e) => {
          if (
            Drupal.speedboxes.config.ignore_elements.includes(
              e.target.tagName.toLowerCase(),
            )
          ) {
            return;
          }

          Drupal.speedboxes.bMouseDown = true;
          Drupal.speedboxes.bMouseMoved = false;
          Drupal.speedboxes.aMouseStartPosition = [e.pageX, e.pageY];

          if (Drupal.speedboxes.pSelectedCheckboxes) {
            Drupal.speedboxes.pSelectedCheckboxes.removeClass(
              'speedboxes-selected',
            );
            Drupal.speedboxes.pSelectedCheckboxes = $();
          }

          if (Drupal.speedboxes.pEditingPopup) {
            Drupal.speedboxes.pEditingPopup.hide();
          }

          return false;
        })
        .on('mousemove scroll', (e) => {
          if (Drupal.speedboxes.bMouseDown) {
            if (!Drupal.speedboxes.pSelectionContainer) {
              Drupal.speedboxes.pSelectionContainer =
                Drupal.speedboxes.createSelectionContainer();
            }

            Drupal.speedboxes.aMouseLastPosition = [e.pageX, e.pageY];
            Drupal.speedboxes.pSelectionRect =
              Drupal.speedboxes.getSelectionRectangle();

            if (!Drupal.speedboxes.bMouseMoved) {
              Drupal.speedboxes.pSelectionContainer.show();
              Drupal.speedboxes.bMouseMoved = true;
            }

            Drupal.speedboxes.pSelectionContainer.css({
              top: `${Drupal.speedboxes.pSelectionRect.top}px`,
              width: `${
                Drupal.speedboxes.pSelectionRect.right -
                Drupal.speedboxes.pSelectionRect.left
              }px`,
              height: `${
                Drupal.speedboxes.pSelectionRect.bottom -
                Drupal.speedboxes.pSelectionRect.top
              }px`,
              left: `${Drupal.speedboxes.pSelectionRect.left}px`,
            });

            Drupal.speedboxes.updateSelectedCheckboxes();
            return false;
          }
        })
        .on('mouseup mouseleave', () => {
          if (!Drupal.speedboxes.bMouseDown) {
            return;
          }

          Drupal.speedboxes.bMouseDown = false;
          if (
            Drupal.speedboxes.pSelectionContainer &&
            Drupal.speedboxes.bMouseMoved
          ) {
            Drupal.speedboxes.pSelectionContainer.hide();

            if (Drupal.speedboxes.pSelectedCheckboxes.length) {
              if (!Drupal.speedboxes.pEditingPopup) {
                Drupal.speedboxes.pEditingPopup =
                  Drupal.speedboxes.createEditingPopup();
              }

              Drupal.speedboxes.pEditingPopup.show();

              const pBody = $('body');
              const iPopupWidth = Drupal.speedboxes.pEditingPopup.outerWidth();
              const iPopupHeight =
                Drupal.speedboxes.pEditingPopup.outerHeight();
              const iBodyWidth = pBody.innerWidth();
              const iBodyHeight = pBody.innerHeight();
              const aPosition = [
                Drupal.speedboxes.aMouseLastPosition[0] + iPopupWidth >
                iBodyWidth
                  ? Drupal.speedboxes.aMouseLastPosition[0] - iPopupWidth
                  : Drupal.speedboxes.aMouseLastPosition[0],
                Drupal.speedboxes.aMouseLastPosition[1] + iPopupHeight >
                iBodyHeight
                  ? Drupal.speedboxes.aMouseLastPosition[1] - iPopupHeight
                  : Drupal.speedboxes.aMouseLastPosition[1],
              ];
              Drupal.speedboxes.pEditingPopup.css({
                left: `${aPosition[0]}px`,
                top: `${aPosition[1]}px`,
              });
              Drupal.speedboxes.updateEditingPopup();
            }
          }

          return false;
        });
    },
  };

  /**
   * Helper function to update selected checkboxes.
   *
   * @return {object}
   *   jQuery object of the selection container.
   */
  Drupal.speedboxes.createSelectionContainer = () => {
    const $selectionContainer = $(
      '<div id="speedboxes-selection" class="speedboxes speedboxes-selection"></div>',
    ).appendTo($('body'));
    $selectionContainer[0].style.position = 'absolute';
    return $selectionContainer;
  };

  /**
   * Helper function to create one editing popup action.
   *
   * @param {string} sAction
   *   The type of action to execute.
   *
   * @return {object}
   *   jQuery object of the action link.
   */
  Drupal.speedboxes.createEditingPopupAction = (sAction) => {
    return $(
      `<a href="#" class="speedboxes-action speedboxes-action-${sAction.replace(
        /([^a-zA-Z0-9-]+)/g,
        '-',
      )}"></a>`,
    )
      .each((index, element) => {
        element.textContent = Drupal.speedboxes.config.localization[sAction];
      })
      .data('speedboxes-action', sAction)
      .mousedown(Drupal.speedboxes.performAction)
      .click(() => {
        return false;
      });
  };

  /**
   * Helper function to create the editing popup.
   *
   * @return {object}
   *   jQuery object of the popup container.
   */
  Drupal.speedboxes.createEditingPopup = () => {
    const $popupContainer = $(
      '<div id="speedboxes-popup" class="speedboxes speedboxes-popup"></div>',
    ).appendTo($('body'));
    $popupContainer[0].style.position = 'absolute';

    Drupal.speedboxes
      .createEditingPopupAction('check_all')
      .appendTo($popupContainer);
    Drupal.speedboxes
      .createEditingPopupAction('uncheck_all')
      .appendTo($popupContainer);
    Drupal.speedboxes
      .createEditingPopupAction('reverse')
      .appendTo($popupContainer);
    return $popupContainer;
  };

  /**
   * Helper function to get the selection rectangle coordinates.
   *
   * @return {object}
   *   Object containing the selection rectangle coordinates.
   */
  Drupal.speedboxes.getSelectionRectangle = () => {
    return {
      top:
        Drupal.speedboxes.aMouseStartPosition[1] >
        Drupal.speedboxes.aMouseLastPosition[1]
          ? Drupal.speedboxes.aMouseLastPosition[1]
          : Drupal.speedboxes.aMouseStartPosition[1],
      right:
        Drupal.speedboxes.aMouseStartPosition[0] >
        Drupal.speedboxes.aMouseLastPosition[0]
          ? Drupal.speedboxes.aMouseStartPosition[0]
          : Drupal.speedboxes.aMouseLastPosition[0],
      bottom:
        Drupal.speedboxes.aMouseStartPosition[1] >
        Drupal.speedboxes.aMouseLastPosition[1]
          ? Drupal.speedboxes.aMouseStartPosition[1]
          : Drupal.speedboxes.aMouseLastPosition[1],
      left:
        Drupal.speedboxes.aMouseStartPosition[0] >
        Drupal.speedboxes.aMouseLastPosition[0]
          ? Drupal.speedboxes.aMouseLastPosition[0]
          : Drupal.speedboxes.aMouseStartPosition[0],
    };
  };

  /**
   * Helper function to update the editing popup.
   */
  Drupal.speedboxes.updateEditingPopup = () => {
    if (!Drupal.speedboxes.pEditingPopup) {
      return;
    }

    if (Drupal.speedboxes.pSelectedCheckboxes.not(':checked').length) {
      Drupal.speedboxes.pEditingPopup
        .find('.speedboxes-action-check-all')
        .removeClass('speedboxes-active-action');
    } else {
      Drupal.speedboxes.pEditingPopup
        .find('.speedboxes-action-check-all')
        .addClass('speedboxes-active-action');
    }

    if (Drupal.speedboxes.pSelectedCheckboxes.filter(':checked').length) {
      Drupal.speedboxes.pEditingPopup
        .find('.speedboxes-action-uncheck-all')
        .removeClass('speedboxes-active-action');
    } else {
      Drupal.speedboxes.pEditingPopup
        .find('.speedboxes-action-uncheck-all')
        .addClass('speedboxes-active-action');
    }
  };

  /**
   * Helper function to perform the selected action.
   *
   * @param {object} e
   *   The event object triggered to perform the action.
   *
   * @return {boolean}
   *   Return false because the action popup is composed of "fake" links.
   */
  Drupal.speedboxes.performAction = (e) => {
    const $pElement = $(e.target);
    const sAction = $pElement.data('speedboxes-action');

    if (sAction === 'check_all') {
      Drupal.speedboxes.pSelectedCheckboxes.attr('checked', 'checked');
    } else if (sAction === 'uncheck_all') {
      Drupal.speedboxes.pSelectedCheckboxes.removeAttr('checked');
    } else if (sAction === 'reverse') {
      // eslint-disable-next-line func-names
      Drupal.speedboxes.pSelectedCheckboxes.each(function () {
        const pSelf = $(this);
        if (pSelf.is(':checked')) {
          pSelf.removeAttr('checked');
        } else {
          pSelf.attr('checked', 'checked');
        }
      });
    }

    Drupal.speedboxes.updateEditingPopup();
    return false;
  };

  /**
   * Helper function to update selected checkboxes.
   */
  Drupal.speedboxes.updateSelectedCheckboxes = () => {
    const pOldSelection = Drupal.speedboxes.pSelectedCheckboxes;
    Drupal.speedboxes.pSelectedCheckboxes = $();

    // eslint-disable-next-line func-names
    $('input:checkbox:enabled:visible').each(function () {
      const pCheckbox = $(this);
      const pPosition = pCheckbox.offset();
      if (
        pPosition.left >= Drupal.speedboxes.pSelectionRect.left &&
        pPosition.left <= Drupal.speedboxes.pSelectionRect.right &&
        pPosition.top >= Drupal.speedboxes.pSelectionRect.top &&
        pPosition.top <= Drupal.speedboxes.pSelectionRect.bottom
      ) {
        Drupal.speedboxes.pSelectedCheckboxes =
          Drupal.speedboxes.pSelectedCheckboxes.add(pCheckbox);
      }
    });

    if (pOldSelection) {
      // For the case we'd like to always show the popup.
      /*
      var pRemoved = pOldSelection
        .not(Drupal.speedboxes.pSelectedCheckboxes)
        .removeClass('speedboxes-selected');

      var pAdded = Drupal.speedboxes.pSelectedCheckboxes
        .not(pOldSelection)
        .addClass('speedboxes-selected');

      if (pRemoved.length || pAdded.length) {
        Drupal.speedboxes.updateEditingPopup();
      }
      */
    } else {
      Drupal.speedboxes.pSelectedCheckboxes.addClass('speedboxes-selected');
    }
  };
})(jQuery, Drupal);
