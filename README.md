# Speedboxes

In many cases when you assign permissions to a role you have to check many
single checkboxes. With this module you just have to select the boxes by "left
click and drag the mouse" over them. Then a small toolbar appears, and you can
check, uncheck or invert the checkboxes state.

On big sites with many roles and permissions this will save a lot of time!


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration.
